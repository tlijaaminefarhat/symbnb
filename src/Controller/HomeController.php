<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller {

    /**
     * 
     * @Route("/hello/{prenom}", name="hellopage")
     * 
     * @return void
     */
    public function hello($prenom) {
        return new Response("hello ". $prenom);
    }

    /**
     *  @Route("/", name="homepage")
     */
    public function home() {

        $prenom = ['aaaa' => 1, 'bbbb' => 2, 'cccc' => 3];
        return $this->render(
            'home.html.twig',
            [
                "title" => "takhrali fih !",
                "age" => 25,
                "tabs" => $prenom
            ]
        );
    }
}

?>