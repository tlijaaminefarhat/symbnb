<?php

namespace App\Form;

use App\Entity\Ad;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AdType extends AbstractType
{
    /**
     * permet d'avoir la confige de base d'un champ
     *
     * @param string $label
     * @param string $placeholder
     * @return void
     */
    private function getConfiguration($label, $placeholder, $propertypath = null) {
        return [
            'label' => $label,
            'attr' => [
                'placeholder' => $placeholder
            ]];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, $this->getConfiguration('Titre', 'Tapé un super titre pour votre annonce !'))
            ->add('slug', TextType::class,  $this->getConfiguration('Adresse web', 'Adresse web automatique'))
            ->add('coverImage', UrlType::class,  $this->getConfiguration('Url de l\'image', 'Donnez l\'adresse d\'une image'))
            ->add('introduction', TextType::class,  $this->getConfiguration('Introduction', 'Donnée un desciption total pour votre annonce'))
            ->add('contents', TextareaType::class, $this->getConfiguration('Desciption detaillée', 'Tapez votre desciption'))
            ->add('rooms', IntegerType::class,  $this->getConfiguration('Nombre de chambres', 'Nombre de chambres disponible'))
            ->add('price', MoneyType::class,  $this->getConfiguration('Prix par nuit', 'Prix voulez par nuit'))
            ->add('save', SubmitType::class, [
                'label' => 'Créer la nouvelle annonce',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
