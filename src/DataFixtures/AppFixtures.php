<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use Faker\Factory;
use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        for ($i=0; $i < 30; $i++) { 
            $ad = new Ad();

            $title = $faker->sentence();
            $converImage = $faker->imageUrl(1000, 350);
            $introduction = $faker->paragraph(2);
            $contents = '<p>' . join('</p><p>', $faker->paragraphs(5)) . '</p>';
            $price = $faker->randomNumber(2);

            $ad->setTitle($title)
                ->setCoverImage($converImage)
                ->setIntroduction($introduction)
                ->setContents($contents)
                ->setPrice($price)
                ->setRooms(mt_rand(1, 5));
                
            for ($j=0; $j <mt_rand(2, 5) ; $j++) { 
                $image = new Image();

                $image  ->setUrl($faker->imageUrl())
                        ->SetCaption($faker->sentence())
                        ->setAd($ad);

                $manager->persist($image);
            }

            $manager->persist($ad);
        }
        $manager->flush();
    }
}
